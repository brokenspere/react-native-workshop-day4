import {createStore ,combineReducers} from 'redux'
import TodoReducer from './TodosReducer'

import CompletesReducer from './CompletesReducer';


const reducer = combineReducers({
    todos: TodoReducer,
    completes: CompletesReducer
})



const store = createStore(reducer);

export default store;